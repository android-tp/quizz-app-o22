package com.example.quiz_app_o22;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity {


    EditText etUsername ,etPassword,etConfirmPwd,etEmail;
    Button bRegister;
    FirebaseAuth firebaseAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        etUsername = (EditText) findViewById(R.id.etUsernameReg);
        etPassword = (EditText) findViewById(R.id.etPasswordReg);
        etConfirmPwd = (EditText) findViewById(R.id.etConfirmPwd);
        etEmail = (EditText) findViewById(R.id.etEmailReg);
        bRegister = (Button) findViewById(R.id.bSignUpReg);
        firebaseAuth=FirebaseAuth.getInstance();

        // Drawable img = etConfirmPwd.getContext().getResources().getDrawable( R.drawable.warning );
        //etConfirmPwd.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_lock_24,0,R.drawable.warning,0);
        bRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mail=etEmail.getText().toString();
                String password=etPassword.getText().toString();

          if(etUsername.getText().toString().length()<20 && etUsername.getText().toString().length()>=4) {
               if(etPassword.getText().toString().length()>8 && isValidPassword(etPassword.getText().toString())){
                   if(etPassword.getText().toString().equals(etConfirmPwd.getText().toString())) {
                       firebaseAuth.createUserWithEmailAndPassword(mail,password )
                               .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                   @Override
                                   public void onComplete(@NonNull Task<AuthResult> task) {

                                       if(task.isSuccessful()){
                                           startActivity(new Intent(getApplicationContext(),MainActivity.class));
                                           finish();
                                       }
                                       else{
                                           Toast.makeText(getApplicationContext(),"E-mail or password is wrong",Toast.LENGTH_SHORT).show();
                                       }

                                   }
                               });

                   }
                   else Toast.makeText(getApplicationContext(),"Password not match",Toast.LENGTH_SHORT).show();


               }
               else{
                   Toast.makeText(getApplicationContext(),"please enter a valid password",Toast.LENGTH_SHORT).show();
               }

           }
           else etUsername.setError("please enter a valid username");

            }
        });




   etPassword.addTextChangedListener(new TextWatcher() {
       @Override
       public void beforeTextChanged(CharSequence s, int start, int count, int after) {

       }

       @Override
       public void onTextChanged(CharSequence s, int start, int before, int count) {

           if(etPassword.getText().toString().length()>8 && isValidPassword(etPassword.getText().toString())){
               etPassword.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_lock_24,0,R.drawable.checked,0);
               bRegister.setEnabled(true);
           }
           else{
               //etPassword.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_lock_24,0,R.drawable.warning,0);3
               Drawable warning = ContextCompat.getDrawable(getApplicationContext(),R.drawable.warning);

               etPassword.setError("Password not match ",warning);
               bRegister.setEnabled(false);
           }
       }

       @Override
       public void afterTextChanged(Editable s) {

       }
   });

   etConfirmPwd.addTextChangedListener(new TextWatcher() {
       @Override
       public void beforeTextChanged(CharSequence s, int start, int count, int after) {

       }

       @Override
       public void onTextChanged(CharSequence s, int start, int before, int count) {
           if(etConfirmPwd.getText().toString().equals(etPassword.getText().toString()) && etPassword.getText().toString().length()>8 && isValidPassword(etPassword.getText().toString())){
               etConfirmPwd.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_check_24,0,R.drawable.checked,0);
               bRegister.setEnabled(true);
           }
           else{
               //etConfirmPwd.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_check_24,0,R.drawable.warning,0);
               Drawable warning = ContextCompat.getDrawable(getApplicationContext(),R.drawable.warning);

               etConfirmPwd.setError("Password not match ",warning);

               bRegister.setEnabled(false);
           }

       }

       @Override
       public void afterTextChanged(Editable s) {

       }
   });




}
    public static boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }
}