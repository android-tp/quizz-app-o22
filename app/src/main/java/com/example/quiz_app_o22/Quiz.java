package com.example.quiz_app_o22;

import androidx.annotation.NonNull;

import com.google.firebase.database.Exclude;

import java.io.Serializable;

public class Quiz implements Serializable  {




    @Exclude private   String id;
    String choiceA;
    String choiceB;
    String choiceC;
    String correctAnswer;
    String pictureID;
    String questionText;
    public Quiz( String choiceA, String choiceB, String choiceC, String correctAnswer, String pictureID, String questionText) {
        this.choiceA = choiceA;
        this.choiceB = choiceB;
        this.choiceC = choiceC;
        this.correctAnswer = correctAnswer;
        this.pictureID = pictureID;
        this.questionText = questionText;
    }
    public Quiz(){}
    /*public void setChoiceA(String choiceA) {
        this.choiceA = choiceA;
    }

    public void setChoiceB(String choiceB) {
        this.choiceB = choiceB;
    }

    public void setChoiceC(String choiceC) {
        this.choiceC = choiceC;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public void setPictureID(String pictureID) {
        this.pictureID = pictureID;
    }

    public void setQustionText(String qustionText) {
        this.qustionText = qustionText;
    }*/
    public void setId(String id) {
        this.id = id;
    }
    public String getChoiceA() {
        return choiceA;
    }

    public String getChoiceB() {
        return choiceB;
    }

    public String getChoiceC() {
        return choiceC;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public String getPictureID() {
        return pictureID;
    }

    public String getQustionText() {
        return questionText;
    }

    @Override
    public String toString() {
        return "Quiz{" +
                "choiceA='" + choiceA + '\'' +
                ", choiceB='" + choiceB + '\'' +
                ", choiceC='" + choiceC + '\'' +
                ", correctAnswer='" + correctAnswer + '\'' +
                ", pictureID='" + pictureID + '\'' +
                ", qustionText='" + questionText + '\'' +
                '}';
    }
}
