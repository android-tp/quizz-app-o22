package com.example.quiz_app_o22;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

import app.futured.donut.DonutProgressView;
import app.futured.donut.DonutSection;

public class ScoreActivity extends AppCompatActivity {

   Button bLogout, bTry;
    int score;


    private DonutProgressView dpvChart;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);
        Intent intent=getIntent();
        score=intent.getIntExtra("score",0) ;

        bLogout=(Button) findViewById(R.id.bExit);
        bTry=(Button) findViewById(R.id.bRety);

        dpvChart = findViewById(R.id.dpvChart);

        DonutSection section = new DonutSection("Section 1 Name", Color.parseColor("#f44336"), score);
        dpvChart.setCap(100f);
        dpvChart.submitData(new ArrayList<>(Collections.singleton(section)));
        bLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Merci de votre Participation !", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
        bTry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ScoreActivity.this,QuizActivity.class));
            }
        });

    }

}


