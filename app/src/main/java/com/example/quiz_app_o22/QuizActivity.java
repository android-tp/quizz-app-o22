package com.example.quiz_app_o22;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class QuizActivity extends AppCompatActivity {

    ImageView imQues;
    RadioGroup rg;
    RadioButton rb;
    TextView tvQues;
    Button bNext;
    int score=0;
    String RepCorrect="Spirit";
    StorageReference storageReference;
    FirebaseDatabase db;
    final  String  TAG="n";



    @Override
    protected void onStart() {
        super.onStart();


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        tvQues=(TextView)findViewById(R.id.tvQuetion);
        rg=(RadioGroup) findViewById(R.id.rgQuiz1);
        bNext=(Button) findViewById(R.id.bNext);
        imQues=(ImageView)findViewById(R.id.ivQuestion);
        db = FirebaseDatabase.getInstance();
       /*DatabaseReference myRef = db.getReference().child("questions");
       final ArrayList<String> List=new ArrayList<>();
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
              // String value = snapshot.getValue(String.class);
              //  Log.d(TAG, "Value is: " + value);
                for (DataSnapshot snapshot:dataSnapshot.getChildren()) {
                    Quiz quiz = snapshot.getValue(Quiz.class);
                    String txt=quiz.getQustionText();

                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        tvQues.setText(List.get(0));*/

       // LoadImage();




        bNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(rg.getCheckedRadioButtonId()==-1){
                    Toast.makeText(getApplicationContext(),"Merci de choisir une réponse S.V.P !",Toast.LENGTH_SHORT).show();
                }
                else {
                    rb=(RadioButton) findViewById(rg.getCheckedRadioButtonId());
                    if(rb.getText().toString().equals(RepCorrect)){
                        score+=1;
                    }
                    Intent intent=new Intent(QuizActivity.this,QuizActivity2.class);
                    intent.putExtra("score",score);
                    startActivity(intent);
                    //overridePendingTransition(R.anim.fadein,R.anim.fadeout);
                    overridePendingTransition(R.anim.exit,R.anim.entry);
                    finish();
                }

            }
        });

    }

   /* public  void LoadImage(Quiz quiz){

        storageReference= FirebaseStorage.getInstance().getReference().child("pictures/TahmKench.jpg");
        try {
            final File file = File.createTempFile("TahmKench","jpg");
            storageReference.getFile(file).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {

                    Toast.makeText(getApplicationContext(),"Picture Retrieved",Toast.LENGTH_SHORT).show();
                    Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                    imQues=(ImageView)findViewById(R.id.ivQuestion);
                    imQues.setImageBitmap(bitmap);

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getApplicationContext(),"Error Occurred",Toast.LENGTH_SHORT).show();

                }
            });
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }


    }*/

    public  void GetAllQuiz()
    {

        //quizzes=new ArrayList<>();
        //db=FirebaseFirestore.getInstance();


      /*  db.collection("quizList").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                if(task.isSuccessful()){
                    for (QueryDocumentSnapshot quiz : task.getResult()) {
                        Log.d(TAG, quiz.getId() + " => " + quiz.getData());
                        Quiz quiz1 =new Quiz(quiz.getString("choiceA"),quiz.getString("choiceB"),quiz.getString("choiceC"),quiz.getString("correctAnswer"),
                                quiz.getString("pictureID"),quiz.getString("questionText"));
                        quizzes.add(quiz1);
                        DocumentReference documentReference = db.collection("quizList").document(quiz.getId());
                        documentReference.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                Quiz quiz =documentSnapshot.toObject(Quiz.class);
                                quizzes.add(quiz);
                                Toast.makeText(getApplicationContext(),quiz.toString(),Toast.LENGTH_SHORT).show();
                            }
                        });



                    }
                   // Toast.makeText(getApplicationContext(),quizzes.size(),Toast.LENGTH_SHORT).show();

                } else {
                    Log.d(TAG, "Error getting documents: ", task.getException());
                }

            }
        });*/


       /* db.collection("quizList").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                if(!queryDocumentSnapshots.isEmpty()){

                    List<DocumentSnapshot> list =queryDocumentSnapshots.getDocuments();

                    for (DocumentSnapshot d : list){
                        Quiz quiz =d.toObject(Quiz.class);
                        quiz.setId(d.getId());
                        quizzes.add(quiz);

                    }
                }
            }
        });*/

    /*    db.collection("quizList").document("Jh9OqUU9yvhVsy8zRT5j").get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {

                if(!documentSnapshot.exists())
                {

                    tvQues.setText(documentSnapshot.getString("questionText"));

                }
                else  Toast.makeText(getApplicationContext(),"Error Occurred",Toast.LENGTH_SHORT).show();

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                   Toast.makeText(getApplicationContext(),"Faild to fetch data",Toast.LENGTH_SHORT).show();
            }
        });
*/
    }

}
